# Aide rapide sur git


Une aide rapide sur l'utilisation de git/git bash
Ceci vous permettra de prendre en main rapidement l'outil Git. Tout en supposant que vous avez déjà le logiciel Git sur votre pc.

## Initialiser le projet

*git init*

## Cloner un depot git

**git remote add nomDuRemote lienDepotGit.**

Ensuite, 

**git clone lienSSHDepotGit**: vous pouvez l'avoir à partir de projet/clone

*Une fois le depôt cloné, il crée un dossier. Entrez dans ce dossier et commencez à travailler*

## Envoyer vos données en ligne (push)

### Ajouter tous les nouveaux fichiers au dépôt git

**git add .**

### Mettre un message aux fichiers ajoutés (obligatoire)

**git commit -m *message***

### Envoyer des données en ligne

*git push origin master*: pour faire un push sur la branche master

